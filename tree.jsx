
'use strict';

import ReactComponentBase from 'z-abs-corelayer-client/react-component/react-component-base';
import React from 'react';
import ReactDOM from 'react-dom';
import 'jquery-ui';
import 'jquery.fancytree/dist/modules/jquery.fancytree.ui-deps.js'
import 'jquery.fancytree/dist/modules/jquery.fancytree.js'


export default class Tree extends ReactComponentBase {
  constructor(props) {
    super(props);
    this.nbr = 0;
    this.activation = false;
    this.expand = false;
    this.tree = null; 
  }
  
  didMount() {
    const self = this;
    this.fancyTree = $("#" + self.props.id).fancytree({
      autoScroll: true,
      checkbox: false,
      selectMode: 1,
      source: self.props.project.source,
      createNode: (event, data) => {
        const node = data.node;
        if(node.isFolder()) {
          let node = data.node;
          if(node.isExpanded()) {
            node.icon = self.getFolderIcon(node.data.types).open;
          }
          else {
            node.icon = self.getFolderIcon(node.data.types).closed;
          }
          node.renderTitle();
        }
        else {
          if(false === node.data._valid) {
            node.icon = self.getFileIcon(node.data.type).invalid;
          }
          else {
            node.icon = self.getFileIcon(node.data.type).valid;
          }
          node.renderTitle();
        }
      },
      init: (event, data) => {
        this.tree = data.tree;
        this.sortChildren();
      },
      beforeActivate: (event, data) => {
        if(undefined !== this.props.activationFilter && !data.node.data.path.startsWith(this.props.activationFilter) && !this.expand) {
          return false;
        }
      },
      beforeExpand: (event, data) => {
        if(!this.props.allowExpand && !this.expand) {
          return false;
        }
        const node = data.node;
        if(node.isExpanded()) {
          node.icon = self.getFolderIcon(node.data.types).closed;
        }
        else {
          node.icon = self.getFolderIcon(node.data.types).open;
        }
        node.renderTitle();
      },
      activate: (event, data) => {
        if(!this.activation) {
          const node = data.node;
          if(!node.isFolder()) {
            self.props.onClickFile(node.key, node.title, node.data.path, node.data.type);
          }
          else {
            self.props.onClickFolder(node.key, node.title, node.data.path, node.data.types);
          }
        }
        return false;
      },
      collapse: (event, data) => {
        self.props.onToggleFolder(data.node.key, false);
      },
      expand: (event, data) => {
        self.props.onToggleFolder(data.node.key, true);
      }
    });
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompareObjectValues(this.props, nextProps);
  }
  
  didUpdate(prevProps, prevState) {
    if(prevProps.project !== this.props.project) {
      this.tree.reload(this.props.project.source).done(() => {
        this.sortChildren();
        this._activate(this.props);
      });
    }
    else {
      this._activate(this.props);
    }
  }
  
  willUnmount() {
    const self = this;
    $("#" + self.props.id).fancytree("destroy");
  }

  render() {
    return (
      <div id={this.props.id}></div>
    );
  }
  
  _activate(nextProps) {
    this.activation = true;
    if(null !== nextProps.current) {
      if('file' === nextProps.current.type) {
        this.tree.activateKey(nextProps.current.file.key);
      }
      else if('folder' === nextProps.current.type) {
        this.expand = true;
        this.tree.activateKey(nextProps.current.folder.key);
        if(nextProps.expandCurrentFolder) {
          this.tree.getNodeByKey(nextProps.current.folder.key).setExpanded(true);
        }
        this.expand = false;
      }
    }
    else {
      this.tree.activateKey(false);
    }
    this.activation = false;
  }
  
  equalNodes(n1, n2) {
    if(n1.length !== n2.length) {
      return false;
    }
    for(let i=0; i < n1.length; ++i) {
      let result = this.equalNode(n1[i], n2[i]);
      if(!result) {
        return result;
      }
    }
    return true;
  }
  
  equalNode(n1, n2) {
    if(n1.title !== n2.title || n1.folder !== n2.folder || n1.key !== n2.key || n1.data.path !== n2.data.path) {
      return false;
    }
    else {
      return this.equalNodes(n1.children !== undefined ? n1.children : [], n2.children !== undefined ? n2.children : []);
    }
  }
  
  sortChildren() {
    this.tree.getFirstChild().sortChildren((a, b) => {
      if(a.folder !== b.folder) {
        return a.folder ? -1 : 1;
      }
      else {
        return a.title >= b.title ? 1 : -1;
      }
    }, true);
  }
  
  getFolderIcon(types) {
    if(undefined === types || 0 === types.length) {
      return {
        open: '/images/icon/folder_normal_open.ico',
        closed: '/images/icon/folder_normal_closed.ico'
      };
    }
    else if(1 === types.length || 'actorjs' === types[0]) {
      let icon = this.props.folderIcons.get(types[0]);
      if(undefined !== icon) {
        return icon;
      }
      else {
        return {
          open: '/images/icon/folder_normal_open.ico',
          closed: '/images/icon/folder_normal_closed.ico'
        };
      }
    }
    else {
      return {
        open: '/images/icon/folder_multi_open.ico',
        closed: '/images/icon/folder_multi_closed.ico'
      };
    }
  }
  
  getFileIcon(type) {
    const icon = this.props.fileIcons.get(type);
    if(undefined !== icon) {
      return icon;
    }
    else {
      return this.props.fileIcons.get('js'); // TODO: add un unknown icon !!!
    }
  }
}

Tree.defaultProps = {
  allowExpand: true
};

Tree.id = 0;
